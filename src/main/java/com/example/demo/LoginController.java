package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * LoginController
 */
@RestController
 public class LoginController {

    @Autowired
    AuthenticationService authenticationService;

    @PostMapping("/login")
    public AuthResponse login(@RequestBody LoginInfo loginInfo){
        return this.authenticationService.authenticate(loginInfo.getUser(), loginInfo.getPassword());
    }
}