package com.example.demo;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * AuthenticationService
 */
@Service
 public class AuthenticationService {
     private RestTemplate restTemplate;

    public AuthenticationService(RestTemplate rest){
        this.restTemplate = rest;
    }

    public AuthResponse authenticate(String user, String password){
        URI uri = URI.create("https://dev-909112.okta.com/oauth2/default/v1/token");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add("Authorization", "Basic MG9hMjdwNG9yNG9lQk1uT3gzNTc6YzM1WXc3Wk40OHF6Mll1Z3MtOGxWeU9DcHhHR3BMX0Y0QmtFYjdleA==");

        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("grant_type", "password");
        bodyMap.add("username", user);
        bodyMap.add("password", password);
        bodyMap.add("scope", "openid");

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(bodyMap, headers);

        ResponseEntity<AuthResponse> response = this.restTemplate.postForEntity(uri, requestEntity, AuthResponse.class);
        return response.getBody();
    }

}