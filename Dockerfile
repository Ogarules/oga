
FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAVA_OPTS
ENV JAVA_OPTS=$JAVA_OPTS
ADD target/demo-0.0.1-SNAPSHOT.jar demo.jar
EXPOSE 4441
ENTRYPOINT exec java $JAVA_OPTS -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8000 -jar demo.jar
# For Spring-Boot project, use the entrypoint below to reduce Tomcat startup time.
#ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar demo.jar
